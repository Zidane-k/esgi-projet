#!/bin/bash

# Copier le fichier de configuration sur le serveur

cp ./config__files/ssh/sshd_config /etc/ssh/sshd_config

cp ./config__files/ssh/Banner /etc/Banner

# Création du compte utilisateur

echo "Veuillez entrer un nom d'utilisateur : \n"
read username
useradd $username
usermod -aG sudo $username

# Création du mot de passe 

echo "Veuillez entrer un mot de passe : \n"
passwd $username

# Stockage de la clef public de l'utilisateur

echo "Veuillez entrer une clé public : \n"
read key
echo $key >> ~/.ssh/authorized_keys

# Redemarrer le service sshd

systemctl restart sshd
